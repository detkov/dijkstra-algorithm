import javafx.util.Pair;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Point> nodes = new ArrayList<>();
        nodes.add(new Point(1, 2));
        nodes.add(new Point(2, 1));
        nodes.add(new Point(4, 1));
        nodes.add(new Point(6, 2));
        nodes.add(new Point(2, 3));
        nodes.add(new Point(3, 3));
        nodes.add(new Point(5, 3));

        ArrayList<Pair<Line2D, Double>> edges = new ArrayList<>();
        edges.add(new Pair<>(new Line2D.Double(nodes.get(0), nodes.get(1)), 8.5));
        edges.add(new Pair<>(new Line2D.Double(nodes.get(1), nodes.get(2)), 7.5));
        edges.add(new Pair<>(new Line2D.Double(nodes.get(2), nodes.get(3)), 8.0));
        edges.add(new Pair<>(new Line2D.Double(nodes.get(0), nodes.get(4)), 10.0));
        edges.add(new Pair<>(new Line2D.Double(nodes.get(4), nodes.get(5)), 10.0));
        edges.add(new Pair<>(new Line2D.Double(nodes.get(5), nodes.get(6)), 2.0));
        edges.add(new Pair<>(new Line2D.Double(nodes.get(6), nodes.get(3)), 1.5));

        MultiList list = new MultiList(nodes, edges);
        Dijkstra d = new Dijkstra(list);
        Pair<ArrayList<Point>, Double> answer = d.dijkstraRMQ(0, 3);
        ArrayList<Point> path = answer.getKey();
        double cost = answer.getValue();
        for (Point node : path){
            System.out.print("x="+node.x+",y="+node.y+" -> ");
        }
        System.out.print(cost);
    }
}
